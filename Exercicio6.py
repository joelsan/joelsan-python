# 1) Crie uma classe que represente um ônibus. O ônibus deverá conter os seguintes atributos:

# capacidade total
# capacidade atual
# movimento

# Os comportamentos esperados para um Ônibus são:
# Embarcar
# Desembarcar
# Acelerar
# Frear

# Lembre-se que a capacidade total do ônibus é de 45 pessoas - não será possível admitir super-
# lotação. Além disso, quando o ônibus ficar vazio, não será permitido efetuar o desembarque
# de pessoas. Além disso, pessoas não podem embarcar ou desembarcar com o onibus em movimento.

# class onibus:

#     def __init__(self):
#         # self.onibus = list()
#         self.capacidadetotal = 45
#         self.capacidadeatual = 0
#         self.movimento = True

#     def embarcar(self, qtd):
#         qtd = int(qtd)
#         if (self.capacidadeatual+qtd) <= self.capacidadetotal and self.movimento == False:
#            self.capacidadeatual += qtd
#            self.acelerar()
#         else:
#             return("Este onibus nao possui lugares disponiveis ou esta em movimento")
    
#     def acelerar(self):
#         self.movimento = True

#     def frear(self):
#         self.movimento = False

#     # def desembarcar(qtd):

# linha=onibus()
# linha.frear()
# print(linha.movimento)
# print(linha.embarcar(52))   #usar PRINT quando deseja imprimir os "returns messages" da class
# print(linha.capacidadetotal)
# print(linha.capacidadeatual)
# linha.acelerar
# print(linha.movimento)

# ==========================================================================
# 2) Implemente um programa que represente uma fila. O contexto do programa é uma
# agência de banco. Cada cliente ao chegar deverá respeitar a seguinte regra: o primeiro
# a chegar deverá ser o primeiro a sair. Você poderá representar pessoas na fila a par-
# tir de números os quais representam a idade. A sua fila deverá conter os seguintes
# comportamentos:

# • Adicionar pessoa na fila: adicionar uma pessoa na fila.
# • Atender Fila: atender a pessoa respeitando a ordem de chegada
# • Dar prioridade: colocar uma pessoa maior de 65 anos como o primeiro da fila

class fila:

    def __init__(self):
        self.pessoas=list()
        self.prioridade=0

    def entra(self):
        idade=int(input("Qual a idade da pessoa? "))
        self.pessoas.append(idade)
        self.mostrafila()

    def sair(self):
        self.prioridade = max(self.pessoas)
        self.pessoas.remove(self.prioridade)
        if self.prioridade > 65:
            return(f"A PRIORIDADE na fila a ser atendida: {self.prioridade} anos")

    def mostrafila(self):
        print(self.pessoas)
        return(f"No momento estamos com {len(self.pessoas)} pessoas na fila")
    
expediente = fila()
expediente.entra()
expediente.entra()
expediente.entra()
print(expediente.sair())
print(expediente.mostrafila())
print(expediente.sair())
print(expediente.mostrafila())
print(expediente.sair())
print(expediente.mostrafila())

