import csv

# ====================================================================================
# 1) Escreva um programa utilizando funções que realize um cadastro.
# Deverão ser coletadas as seguintes informações:

# CPF
# Nome
# Idade
# Sexo
# Cidade

# Os registros deverão ser armazenados em um arquivo CSV.
# Para manter o padrão brasileiro, o CSV será separado pelo caractere ";".
# O programa deverá possuir uma função de consulta e de exclusão (POR NOME OU CPF).
# O programa deverá possuir tratamentos de erro, com a finalidade de que o programa nunca
# dê uma exceção e também que ele não aceite dados incorretos em nenhum momento.

# 1: Importar os modulos a serem usados.
# 2: Criamos uma função para o menu principal do programa.
# 3: Criar uma função para coletar as informações do cliente.
# 4: Criar uma função para validar as informações passadas pelo cliente.
# 5: Criar uma função para adicionar essas informações em um arquivo CSV.
# 6: Criar uma função para consultar o arquivo CSV.
# 7: Criar uma função para remover entradas do arquivo CSV.


while True:
    opção = input("Gostaria de cadastrar uma nova entrada no banco? [S/N]: ")

    if opção.upper() == "N":
        print("Parando o programa...")
        break

    elif opção.upper() == "S":
        cpf = input("digite o CPF: ")
        nome = input("digite o nome: ")
        idade = input("digite a idade: ")
        sexo = input("digite o sexo: ")
        endereco = input("digite o endereço: ")
        cidade = input("digite a cidade: ")

        pessoa = (cpf, nome, idade, sexo, endereco, cidade)

        with open("banco.csv", "a", newline="") as arquivo:
            conteudo = csv.writer(arquivo, delimiter=";")
            conteudo.writerow(pessoa)

    else:
        print("Opção inválida")
