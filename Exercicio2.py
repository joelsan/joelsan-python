# 1) Escreva um script em Python que receba dois números e que seja realizado as seguintes
# operações:
# • soma dos dois números
# • diferença dos dois números
# O resultado deverá ser apresentado conforme a seguir - no exemplo foram digitados os números
# 4 e 2:

# numero1 = input("Entre com o primeiro numero:")
# numero2 = input("Entre com o segundo numero: ")

# ------------------------------
# Soma: 4 + 2 = 6
# Diferença: 4 - 2 = 2

# print(f"A soma dos dois numeros: {int(numero1)+int(numero2)}")
# print(f"A diferenca entre eles : {int(numero1)-int(numero2)}")


# 2) Escreva um script em Python que substitua o caractere “x” por espaço considerando a
# seguinte frase:
# “Umxpratoxdextrigoxparaxtrêsxtigresxtristes”

# frase = 'Umxpratoxdextrigoxparaxtrêsxtigresxtristes'.replace("x"," ")
# print(frase)

# 3) Escreva um programa que receba o ano de nascimento, e que ele retorne à geração
# a qual a pessoa pertence. Para definir a qual geração uma pessoa pertence temos a
# seguinte tabela:

# nascimento=int(input("Ano de nascimento: "))

# Geração        Intervalo

# Baby Boomer -> Até 1964
# if nascimento <= 1964:
#     print("Voce pertence a geracao BABY BOOMER!")

# # Geração X   -> 1965 - 1979
# elif nascimento >= 1965 and nascimento <= 1979:
#     print("Voce pertence a geracao X!")

# # Geração Y   -> 1980 - 1994
# elif nascimento >= 1980 and nascimento <= 1994:
#     print("Voce pertence a geracao Y!")

# # Geração Z   -> 1995 - Atual
# else:
#     print("Voce pertence a geracao Z!")

# Para testar se seu script está funcionando, considere os seguintes resultados esperados:

# • ano nascimento = 1988: Geração: Y
# • ano nascimento = 1958: Geração: Baby Boomer
# • ano nascimento = 1996: Geração: Z

# =================================================================
# 4) Escreva um script em python que represente uma quitanda. O seu programa deverá
# apresentar as opções de frutas, e a cada vez que você escolher a fruta desejada, a fruta
# deverá ser adicionada a uma cesta de compras.

# Menu principal:

# Quitanda:
# 1: Ver cesta
# 2: Adicionar frutas
# 3: Sair

# Menu de frutas:
# Digite a opção desejada:
# Escolha fruta desejada:
# 1 - Banana
# 2 - Melancia
# 3 - Morango

# Digite à opção desejada: 2
# Melancia adicionada com sucesso!

# Os menus 1 e 2 deverão retornar ao menu principal após executar as suas tarefas.
# Você deverá validar as opções digitadas pelo usuário (caso ele digitar algo errado, a mensagem:
# Digitado uma opção inválida

# O programa deverá ser encerrado apenas se o usuário digitar a opção 3.

Quitanda=True
bananas=0
melancias=0
morangos=0

while Quitanda:
    print(""" 
    ===   Quitanda  ===
    1: Ver Cesta
    2: Adicionar Frutas
    3: Sair
    """)
 
    opcao = input("Opcao [1/2] ou 3 [Sair] ")

    if int(opcao) == 3:
        # Quitanda = False
        break

    elif int(opcao) == 1:
        print(f"""
            === Ver Cesta ===
            Bananas   : {bananas}
            Melancias : {melancias}
            Morangos  : {morangos}
              """)  

    elif int(opcao) == 2:
        qtd  = 0
        fruta=int(input(f"""
            === Adicionar ===
            1 - Bananas
            2 - Melancias
            3 - Morangos
            =================  
            Selecione: 
              """))
        qtd=int(input(f"Quantidade: "))
        
        if int(fruta) == 1:
            bananas = bananas + qtd
        elif int(fruta) == 2:
            melancias = melancias + qtd
        elif int(fruta) == 3:
            morangos = morangos + qtd

        