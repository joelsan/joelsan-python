# Funcoes
def saudacao(nome):
    print(f"Bem vindo {nome}")

def calculadora():
    num1 = int(input("Informe o primeiro numero: "))
    num2 = int(input("Informe o segundo  numero: "))

    oper = int(input("""
                --> Selecione:
                 1 - Soma
                 2 - Diferenca
                 3 - Multiplicacao
                 4 - Divisao

                 """))
    if oper == 1:
        print(f"A Soma entre {num1} e {num2} = {num1+num2}")
    elif oper == 2:
        print(f"A Diferenca entre {num1} e {num2} = {num1-num2}")
    elif oper == 3:
        print(f"A Multiplicacao entre {num1} e {num2} = {num1*num2}")
    elif oper == 4:
        print(f"A Divisao entre {num1} e {num2} e {num1/num2}")
    else:
        print("Opcao invalida...")

def pares(x):
    par = 0
    for num in x:
        if num%2 == 0:
            par += 1
    
    return par

# Exercicio 1:
# Escreva uma função que receba um nome e que tenha como saída uma saudação.
# O argumento da função deverá ser o nome.

#nome = input("Qual o seu nome? ")
#saudacao(nome)

# ====================================================
# Exercicio 2:
# Escreva uma calculadora utilizando funções
# Ela pergunta dois numeros, e da as opções de calculo.
# (soma, diferença, multiplicação, divisão)

#calculadora()

# ====================================================
# Exercicio 3:
# Escreva um programa que possua uma função que conte o
# numero de números pares passados à ela, pelo usuário.

#numeros =[]

#while True:

#    numeros.append(int(input(f"Numero: ")))

#    if input("Mais um numero? [s/n] ") == "s": 
#        continue

#    print(f"A lista contem {pares(numeros)} numeros pares!")

#    break


# ====================================================
# Exercicio 4:
# Assumindo que uma lata de tinta pinta 3m², escreva um programa
# que possua uma função que receba as dimenções de uma parede,
# passadas pelo usuario, calcule sua área, e mostre uma mensagem
# dizendo quantas latas de tinta seriam necessárias para pintar
# essa parede.

def pintar(x,y):
    return (x*y)/3

altura = int(input("Informe a altura: "))
largura= int(input("Informe a largura:"))

print(f"Serao necessarias {pintar(altura,largura)} latas de tinta!")