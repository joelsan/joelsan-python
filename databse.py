# Arquivo usado pra armazenar as funções relacionadas ao banco de dados.
# mover para LIB futuramente

#import from lib import classes
import sqlite3

def criar_banco():
    conexao = sqlite3.connect(r"data\database.db")
    cursor = conexao.cursor()

    cursor.execute("CREATE TABLE IF NOT EXISTS clientes (nome PRIMARY KEY, idade, saldo)")
    cursor.execute("CREATE TABLE IF NOT EXISTS auth (nome PRIMARY KEY, usuario, senha)")
    conexao.commit()
    conexao.close()

def registrar():
    nome = input("Qual o nome completo da pessoa a ser cadastrada? ")
    idade = input("Qual a idade dessa pessoa? ")
    saldo = 0.00
    usuario = input("Qual o nome de usuario? ")
    senha = input("Escolha uma senha: ")

    pessoa = classes.Cliente(nome, idade, saldo, usuario, senha)
    pessoa.cadastrar()

def login():
    login_usuario = input("Digite seu usuario: ")
    login_senha = input("Digite sua senha: ")

    conexao = sqlite3.connect(r"data\database.db")
    cursor = conexao.cursor()

    cursor.execute(f"SELECT senha FROM auth WHERE usuario = '{login_usuario}'")
    conteudo = cursor.fetchone()

    conexao.close()

    if login_senha == conteudo[0]:
        return True
    return False

def saldo(nome):
    conexao = sqlite3.connect(r"data\database.db")
    cursor = conexao.cursor()

    cursor.execute(f"SELECT saldo FROM clientes WHERE nome = '{nome}'")
    conteudo = cursor.fetchone()

    conexao.close()
    print(conteudo[0])

def transferencia(nome):
    acao = input("Que tipo de transferencia gostaria de realizar? [deposito/saque]: ")
    valor = input("Qual o valor da transferencia? ")

    conexao = sqlite3.connect(r"data\database.db")
    cursor = conexao.cursor()

    cursor.execute(f"SELECT saldo FROM clientes WHERE nome = '{nome}'")
    saldo_usuario = cursor.fetchone()
    saldo_usuario = float(saldo_usuario[0])

    if acao == "deposito":
        novo_saldo = saldo_usuario + valor
    elif acao == "saque":
        novo_saldo = saldo_usuario - valor

    cursor.execute(f"UPDATE clientes SET saldo = {novo_saldo} WHERE nome = '{nome}'")
    conexao.commit()
    conexao.close()

    print("Transferencia efetuada.")
