# Crie um script para simular uma lanchonete.
# 
# - Essa lanchonete deverá ter 5 tipos de comida e 3 tipos de bebidas no cardápio.
# - Quando clientes chegarem a essa lanchonete, eles deverão fazer um pedido aleatório de 1 comida + 1 bebida.
# - A lanchonete deverá ter um numero limitado de porções dessas comidas/bebidas. Comida = 5, bebida = 7.
# - Quando o cliente fazer o pedido, ele deverá ser notificado se as opções escolhidas estão disponiveis,
# e quais seus preços.

# Criar um DB.
# Criar uma tabela pra comida e uma pra bebida.
# Preencher essas tabelas.

# Criar uma função pra simular um cliente comprando algo.


import sqlite3

conexao = sqlite3.connect("restaurante.db")
cursor = conexao.cursor()

criar_tabela_comidas = """ 
CREATE TABLE IF NOT EXISTS comidas (
id integer primary key autoincrement,
descricao text,
valor float,
disponiveis integer 
)"""

mostrar_tabela_comidas = """
SELECT * from comidas
"""

cursor.execute(criar_tabela_comidas)

def comando_adicionar_comida(nome, valor, disponiveis):
    adicionar_comidas = f"""
    INSERT INTO comidas (descricao, valor, disponiveis)
    VALUES ('{nome}', {valor}, {disponiveis})
    """
    cursor.execute(adicionar_comidas)


comando_adicionar_comida("Salgado", 8.50, 100)
comando_adicionar_comida("Lanche", 18.50, 20)
comando_adicionar_comida("Refeicao", 22.50, 10)
comando_adicionar_comida("Sobremesa", 15.80, 15)
comando_adicionar_comida("Petiscos", 10, 50)

cursor.execute(mostrar_tabela_comidas)
exibicao = cursor.fetchall()

for linha in exibicao:
    print(linha)
