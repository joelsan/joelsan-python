# # PRINT e um comando utilizado para exibir mensagens na tela.

# print("Ola, bem vindo ao curso de Python")

# # INPUT e um comando utilizado para coletar informacoes

# input("Qual e o seu nome? ")

# #VARIAVEIS sao palavras que armazenam um valor.

# nome = "Joel A Santos"
# print(nome)

# idade = input("Qual a sua idade? ")
# print(idade)


# Arquivo principal do programa, contém o menu inicial.
import database

def menu_principal(nome):
    while True:
        acao = input("""Que ação gostaria de efetuar?
1 - Checar o saldo
2 - Realizar uma transferencia
3 - Sair
""")
        if acao == "1":
            database.saldo(nome)
        elif acao == "2":
            database.transferencia(nome)
        elif acao == "3":
            break
        else:
            print("Opção inválida")

def menu_inicial():
    database.criar_banco()
    
    while True:
        escolha = input("""
    Bem vindo ao caixa eletrônico:
                        
    1 - Se registrar no banco.
    2 - Se logar no sistema.
    """)
        
        if escolha == "1":
            database.registrar()

        elif escolha == "2":
            validacao = database.login()
            if validacao != False:
                menu_principal(validacao)

        else:
            print("Valor inválido")


if __name__ == "__main__":
    menu_inicial()
