# 1) Escreva um programa em Python que simule uma dança das cadeiras. Você deverá
# importar o modulo random e iniciar uma lista com nomes de pessoas que participariam da
# brincadeira. O jogo deverá iniciar com 9 cadeiras e 10 participantes. A cada rodada,
# uma cadeira deverá ser retirada e um dos jogadores, de forma aleatória, ser eliminado. O
# jogo deverá terminar quando apenas restar uma cadeira e ao final de todas as rodadas,
# deverá ser apresentado vencedor.

# Dica: [OPCIONAL] Você poderá utilizar o modulo "time" para simular um tempo de a cada rodada para criar
# um efeito mais interessante.

# Dica: [OPCIONAL] Tentem fazer a remoção de uma pessoa aleatória por rodada sem utilizar a função "choice".

# import random
# import time

# participantes = []
# print("Informe os 10 nomes dos participantes")

# for x in range(0,10):
#     participantes.append(input(f"Nome do {x+1} participante: "))

# print(participantes)

# for x in range(9):
#     saira=random.choice(participantes)
#     print(f"Saindo o participante {saira}")
#     time.sleep(2)
#     participantes.remove(saira)

# print(f"O vencedor foi {participantes}")

# =============================================================
# 2) Crie um programa que pergunte para o usuario um numero de pessoas a participarem de um sorteio (2-20),
#  e o numero de pessoas a serem sorteadas, e depois sorteie esse numero de pessoas da lista.

# while True:
#     pessoas = int(input("Quantas pessoas participarao do sorteio? [2-20] "))
#     if pessoas > 1 and pessoas < 21:
#         break

# sorteadas = int(input("Serao quantos vencedores: "))

# O programa deverá pegar o numero de pessoas a participar aleatoriamente desta lista:

# lista = ["Joao", "Maria", "Tiago", "Amanda", "Emanuele", "Caio", "Suzana", "Miguel", 
# "Rosangela", "Rian", "Lucimar", "Ulisses", "Leonardo", "Kaique", "Bruno", "Raquel", 
# "Benedito", "Tereza", "Valmir", "Joaquim"]

# participantes=[]
# vencedores=[]

# participantes=random.sample(lista,pessoas)
# vencedores=random.sample(participantes,sorteadas)

# print(participantes)
# print(vencedores)
 

# Nota: A mesma pessoa não pode ganhar duas vezes.


# =============================================================
# 3) Escreva um programa em python que conte as vogais que a música ‘Faroeste Caboclo’
# tem em sua letra. Armazena a letra da música em um arquivo do tipo txt.
# Dica: Não se esqueça de considerar as letras maiúsculas, minúsculas e com acentuação.

# with open("faroeste.txt","r",encoding="UTF-8") as arquivo:
#     letra = arquivo.read()
#     print(letra)


# ====================================================================================
# 4) Escreva um programa em python que realize um cadastro. Deverão ser coletadas as
# seguintes informações:

CPF = 0
Nome = ""
Idade=0
Sexo=""
Endereco=""

input(f"""
# CPF {CPF}
# Nome {Nome}
# Idade {Idade}
# Sexo {Sexo}
# Endereço {Endereco}
      """)

      
# Os registros deverão ser armazenados em um arquivo CSV. Caso desejar manter o padrão
# brasileiro, o CSV será separado pelo caractere ;
